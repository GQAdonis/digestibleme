import 'package:digestableme/widget/scaffold/layout_selector.dart';
import 'package:digestableme/widget/ios/splitview/splitview_layout.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_test/flutter_test.dart';

import '../test/fixtures/adapters/device_adapter_iphone_stub.dart';
import '../test/fixtures/build.dart';

void main() {
  group('iPhone - Home Screen: ', () {
    showRecipeDetails();
  });
}

Widget buildLayout() {
  return Build.app(const LayoutSelector(), deviceAdapter: DeviceAdapterIphoneStub());
}

Future<void> showRecipeDetails() async {
   return testWidgets('When selecting a digest item like a recipe from the list the recipe details are displayed', (WidgetTester tester) async {
      await tester.pumpWidget(buildLayout());
      await tester.tap(find.text('How to boil an egg'));
     
      var navBarFinder = find.descendant(
        of: find.byType(SplitViewContent),
        matching: find.text("Take a small saucepan with a glass lid and carefully place a single egg"));
      expect(navBarFinder, findsOneWidget);
   });
}
