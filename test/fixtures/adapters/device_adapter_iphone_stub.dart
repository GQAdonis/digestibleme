/*
  Device Adapter Stub
 */
import 'package:digestableme/adapter/device_adapter.dart';
import 'package:digestableme/model/config/device.dart';
import '../../unit/model/config/device/device_test/fixture/device_fixture.dart';

class DeviceAdapterIphoneStub implements DeviceAdapter{
  // Use the current device info to 
  @override
  Future<Device> getDevice() async {
    return DeviceFixtureIPhone.buildDevice();
  }   
}