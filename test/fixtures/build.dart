import 'package:digestableme/adapter/device_adapter.dart';
import 'package:digestableme/data/memory_repository.dart';
import 'package:digestableme/model/layout.dart';
import 'package:digestableme/model/tab/tab_item.dart';
import 'package:digestableme/theme/themer.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

class TestProviders {
  static ChangeNotifierProvider<Themer> themer({
    Brightness? brightness = Brightness.light,
  }) =>
      ChangeNotifierProvider<Themer>(
        create: (_) => Themer(
          brightnessValue: brightness,
        ),
        lazy: false,
      );

  static ChangeNotifierProvider<Layout> layout() =>
      ChangeNotifierProvider<Layout>(
        create: (_) => Layout(),
        lazy: false,
      );

  static ChangeNotifierProvider<MemoryRepository> memoryRepo() =>
      ChangeNotifierProvider<MemoryRepository>(
        create: (_) => MemoryRepository(),
        lazy: false,
      );
}

class Build {
  static Widget app(
    Widget child, {
    Brightness? brightness,
    DeviceAdapter? deviceAdapter,
    List<SingleChildWidget>? providers,
  }) {
    List<SingleChildWidget> defaultProviders = [
      TestProviders.themer(brightness: brightness),
      TestProviders.layout(),
      TestProviders.memoryRepo(),
    ];
    return MultiProvider(
      providers: providers ?? defaultProviders,
      child: Builder(
        builder: (_) => TestDigestableMe(
          child,
          brightness: brightness,
          deviceAdapter: deviceAdapter,
        ),
      ),
    );
  }

  static List<TabItem> buildTabItems(int number) {
    var tabItems = <TabItem>[];
    for (var i = 0; i < number; i++) {
      tabItems.add(TabItem(
        label: "label" + i.toString(),
        icon: const Icon(CupertinoIcons.group),
        content: Container(
          key: Key("TabbedLayoutContentArea" + i.toString()),
          color: CupertinoColors.activeBlue,
        ),
        globalKey: GlobalKey<NavigatorState>(),
      ));
    }
    return tabItems;
  }
}

class TestDigestableMe extends StatelessWidget {
  final Widget sut;
  final Brightness? brightness;
  final DeviceAdapter? deviceAdapter;
  const TestDigestableMe(this.sut,
      {Key? key, this.brightness, this.deviceAdapter})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (deviceAdapter != null) {
      deviceAdapter!.getDevice().then(
            (device) => {
              Provider.of<Layout>(context, listen: false).setDevice(device),
              Provider.of<Themer>(context, listen: false).setThemeAndNotify(
                brightnessValue: brightness ?? Brightness.light,
                whiteLabelValue: WhiteLabel.none,
              ),
            },
          );
    }

    return CupertinoApp(
      title: 'DigestableMeTest',
      theme: Provider.of<Themer>(context).themeData,
      home: sut,
    );
  }
}
