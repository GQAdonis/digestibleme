/*
List Item - tests.

Acceptance Criteria:
1. Has a left aligned title image.
2. Has a recipe title, right of the title image.
3. Displays the authors name below the title.
*/

// Flutter imports:
import 'package:cupertino_list_tile/cupertino_list_tile.dart';
import 'package:digestableme/model/list/list_item_info.dart';
import 'package:digestableme/widget/scaffold/avatar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_test/flutter_test.dart';

// Project imports:
import 'package:digestableme/widget/scaffold/list_item.dart';

import '../../fixtures/build.dart';

const String title = "Item title";
const String subtitle = "Item subtitle";
const Icon addIcon = Icon(CupertinoIcons.add_circled_solid);
// ignore: prefer_function_declarations_over_variables
GestureTapCallback? onTap = () => "Ouch!";

Widget buildWidget() {
  var listItemInfo = ListItemInfo(
    leading: Avatar(Image.asset("assets/recipes_sm3.jpg")),
    title: const Text(title),
    subtitle: const Text(subtitle),
    trailing: addIcon,
    onTap: onTap,
  );

  return Build.app(ListItem(listItemInfo));
}

void main() {
  group('List Item: ', () {
    isCupertinoListTile();
    displaysLeadingWidget();
    displaysTitle();
    displaysSubTitle();
    displaysTrailingWidget();
    setsOnTapFunction();
  });
}

Future<void> isCupertinoListTile() async {
  return testWidgets(
      'Uses the Cupertino version of a ListTile to give a native list item experience on iOS devices.',
      (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget());
    expect(find.byType(CupertinoListTile), findsOneWidget);
  });
}

Future<void> displaysLeadingWidget() async {
  return testWidgets(
      'Displays a leading widget on the left of the tile, typically a logo or avatar.',
      (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget());
    expect(find.byType(CircleAvatar), findsWidgets);
  });
}

Future<void> displaysTitle() async {
  return testWidgets(
      'Shows the list item title to the left of the leading widget.',
      (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget());
    expect(find.text((title)), findsOneWidget);
  });
}

Future<void> displaysSubTitle() async {
  return testWidgets('Shows the sub-title under the title.',
      (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget());
    expect(find.text((subtitle)), findsOneWidget);
  });
}

Future<void> displaysTrailingWidget() async {
  return testWidgets(
      "Displays a trailing widget on the right, typical used for actions like add '+', or badges.",
      (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget());
    expect(find.byWidget(addIcon), findsOneWidget);
  });
}

Future<void> setsOnTapFunction() async {
   return testWidgets('Sets the OnTap function so that the list item will respond to user when tapped.', (WidgetTester tester) async {
      await tester.pumpWidget(buildWidget());
      var listTile = tester.widget<CupertinoListTile>(find.byType(CupertinoListTile));
      expect(listTile.onTap, onTap);
   });
}
