import 'package:digestableme/model/splitview/side_menu.dart';
import 'package:flutter/cupertino.dart';

class SideMenuFixture{
  static List<MenuItem> buildMenuItems(int number) {
    var menuItem = <MenuItem>[];
    for (var i = 0; i < number; i++) {
      
      menuItem.add(
          MenuItem(const Icon(CupertinoIcons.group), "label" + (i + 1).toString()));
    }
    return menuItem;
  }
}