/*
   SplitviewModel
*/

import 'package:digestableme/model/splitview/splitview.dart';
import 'package:flutter_test/flutter_test.dart';

import 'fixture/side_menu_fixture.dart';

// #######
//    #    ######  ####  #####
//    #    #      #        #
//    #    #####   ####    #
//    #    #           #   #
//    #    #      #    #   #
//    #    ######  ####    #

void main() {
    group('Splitview - Model: ', () {
        hasSelectedIndex();
        zeroWhenFirstMenuItemIsSelected();
        oneWhenSecondMenuItemIsSelected();
        oneHundredWhenThe101stMenuItemIsSelected();
    });
}

Splitview buildSplitviewModel({int? numberOfMenuItems}){
  return Splitview(menuItems: SideMenuFixture.buildMenuItems(numberOfMenuItems ?? 3));
}

Future<void> hasSelectedIndex() async {
  return test("Has a selected index that is set to the position of the selected menu item e.g. if 'Friends' is the second menu item it is set to 1 (0 Based index).", () {
    var model = Splitview();
    expect(model.selectedIndex, 0);
  });
}

Future<void> zeroWhenFirstMenuItemIsSelected() async {
  return test("SelectedIndex: Set to 0 when the first menu item is selected.", () {
    var model = buildSplitviewModel();
    model.selectedMenuItem = "label1";
    expect(model.selectedIndex, 0);
  });
}

Future<void> oneWhenSecondMenuItemIsSelected() async {
  return test("SelectedIndex: Set to 1 when the 2nd menu item is selected.", () {
    var model = buildSplitviewModel();
    model.selectedMenuItem = "label2";
    expect(model.selectedIndex, 1);
  });
}

Future<void> oneHundredWhenThe101stMenuItemIsSelected() async {
  return test("SelectedIndex: Set to 100 when the 101st menu item is selected.", () {
    var model = buildSplitviewModel(numberOfMenuItems: 101);
    model.selectedMenuItem = "label101";
    expect(model.selectedIndex, 100);
  });
}