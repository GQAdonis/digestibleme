/*
  SideMenu - Well polished useful widget, brilliantly coded with care.
*/
// #     #                                 #######
// #  #  # # #####   ####  ###### #####       #    ######  ####  #####
// #  #  # # #    # #    # #        #         #    #      #        #
// #  #  # # #    # #      #####    #         #    #####   ####    #
// #  #  # # #    # #  ### #        #         #    #           #   #
// #  #  # # #    # #    # #        #         #    #      #    #   #
//  ## ##  # #####   ####  ######   #         #    ######  ####    #

import 'package:digestableme/core/string/string_extension.dart';
import 'package:digestableme/event/menu/menu_item_tapped.dart';
import 'package:digestableme/model/splitview/side_menu.dart';
import 'package:digestableme/widget/ios/splitview/side_menu.dart';
import 'package:digestableme/widget/ios/splitview/side_menu_header.dart';
import 'package:digestableme/widget/ios/splitview/side_menu_items.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../../fixtures/build.dart';
import '../../../scaffold/finders.dart';
import 'fixture/side_menu_fixture.dart';

// #######
//    #    ######  ####  #####
//    #    #      #        #
//    #    #####   ####    #
//    #    #           #   #
//    #    #      #    #   #
//    #    ######  ####    #

const String headerLabel = "Digestable Me";

Widget buildWidget({Brightness? brightness, SideMenuVM? viewmodel}) {
  return Build.app(
    SideMenu(viewmodel ?? SideMenuVM(headerLabel, SideMenuFixture.buildMenuItems(3))),
    brightness: brightness,
  );
}

// Run
void main() {
  group('SideMenu: ', () {
    hasHeader();
    hasMenuItems();

    headerIsBox();
    canSetHeaderLabel();
    headerHasTransparentBackground();
    
    menuItemHasIcon();
    menuItemHasLabel();
    menuItemNotifiesWhenSelected();
  });
}

Future<void> hasHeader() async {
  return testWidgets('Has a header area above the menu items.',
      (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget());
    expect(find.byType(SideMenuHeader), findsOneWidget);
  });
}

Future<void> hasMenuItems() async {
  return testWidgets('Displays the menu items below the header.',
      (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget());
    expect(find.byType(SideMenuItems), findsOneWidget);
  });
}

Future<void> headerIsBox() async {
  return testWidgets('Header: Is a box that uses the full width.',
      (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget());
    var sideBarHeader = findFirstDescendent<SizedBox>(tester, SideMenuHeader);
    expect(sideBarHeader.width, double.infinity);
  });
}

Future<void> canSetHeaderLabel() async {
  return testWidgets(
      'Header: Displays a label describing the menu, often the application name.',
      (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget());
    var labelFinder = find.descendant(
        of: find.byType(SideMenuHeader), matching: find.text(headerLabel));
    expect(labelFinder, findsOneWidget);
  });
}

Future<void> headerHasTransparentBackground() async {
  return testWidgets('Header: Has a transparent background.',
      (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget());
    var decoration =
        findFirstDescendent<DecoratedBox>(tester, SideMenuHeader).toString();
    expect(decoration.containsIgnoreCase("Color(0x00000000)"), true);
  });
}

Future<void> menuItemHasIcon() async {
  return testWidgets('Menu Item: Has an Icon on the left',
      (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget());
    var menuItemIcons = find.descendant(
        of: find.byType(SideMenuItems), matching: find.byType(Icon));
    expect(menuItemIcons, findsNWidgets(3));
  });
}

Future<void> menuItemHasLabel() async {
  return testWidgets('Menu Item: Has a label to the right of the Icon',
      (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget());
    var menuItemIcons = find.descendant(
        of: find.byType(SideMenuItems), matching: find.byType(Text));
    expect(menuItemIcons, findsNWidgets(3));
  });
}

Future<void> menuItemNotifiesWhenSelected() async {
  return testWidgets(
      'Menu Item: When tapped notifies the widgets above it, so they can take action, like switching content.',
      (WidgetTester tester) async {
    bool isNotified = false;
    const String label = "label1";

    // var sut = Build.app(
    //   NotificationListener<MenuItemTapped>(
    //     child: SideMenu(SideMenuVM(headerLabel, buildMenuItems(3))),
    //     onNotification: (notification) => isNotified = notification.label == label,
    //   ),
    // );

    var sut = Build.app(
      NotificationListener(
        child: SideMenu(SideMenuVM(headerLabel, SideMenuFixture.buildMenuItems(3))),
        onNotification: (notification) => isNotified = isNotified = notification.runtimeType == MenuItemTapped && (notification as MenuItemTapped).label == label,
      ),
    );

    await tester.pumpWidget(sut);
    await tester.tap(find.text(label));
    await tester.pump();

    expect(isNotified, true);
  });
}
