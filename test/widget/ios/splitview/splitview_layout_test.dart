/*
Splitview Layout

Done
- sidbar column left
  - Has action bar
    - With icon to collaspe it.
  - Has nav list, top and second level, can handle more levels ?
    - Digest
      - Recipes
    - Friends
    - History
- List column center
  - Has Action bar
    - With icon to edit
  - Has Large title
  - Has Sub title
  - Has List of items
- content column right, uses remaining space
  - Has Action bar
    - With icon to delete item
  - Content
*/

// Package imports:
import 'package:digestableme/core/color/color_extension.dart';
import 'package:digestableme/core/string/string_extension.dart';
import 'package:digestableme/theme/light/cupertino_light_theme.dart';
import 'package:digestableme/model/splitview/splitview.dart';
import 'package:digestableme/widget/ios/splitview/side_menu_items.dart';
import 'package:digestableme/widget/ios/splitview/splitview_layout.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:provider/provider.dart';

import '../../../fixtures/build.dart';
import '../../../scaffold/finders.dart';
import 'fixture/side_menu_fixture.dart';

void main() {
  group('Splitview Widget (iPad): ', () {
    hasThreeColumns();
    hasSideBar();
    hasSupplementaryArea();
    hasContentArea();
    listensForMenuItemTapEvents();

    sideBarBackgroundIsLightGrey();
    sideBarBackgroundIsDarkGreyInDarkMode();
    sideBarHasThinRightBorder();
    sideBarDisplaysMenuItems();

    supplementaryColumnHasANavBar();
    supplementaryColumnNavBarHasALabel();
    supplementaryColumnNavBarIsExtraLightGrey();
    supplementaryColumnNavBarWithoutContentIsHidden();

    supplementaryColumnDisplaysTheContent();
    usesAnIndexedStackToSwitchSupplementaryContentOut();

    theContentUsesRemainingSpace();
    contentColumnDisplaysTheContent();
    contentBackgroundIsGrey();
    usesAnIndexedStackToSwitchContentOut();

    assertsContentWidgetsEqualMenuItems();
    assertsSupplementaryContentWidgetsEqualMenuItems();
    assertsSupplementaryNavigationWidgetsEqualMenuItems();
  });
}

const String supplementaryNavBarLabel = "Recipes";
const Widget supplementaryNavBarMiddle = Text(supplementaryNavBarLabel);

Widget buildWidget({Brightness? brightness, Splitview? splitviewModel}) {
  splitviewModel ??= buildViewModel();

  return Build.app(
    ChangeNotifierProvider(
      create: (context) => splitviewModel!.initialise(context),
      child: const SplitviewLayout(),
    ),
    brightness: brightness,
  );
}

Splitview buildViewModel({int numberOfMenuItems = 2, int numberOfContentWidgets = 2, int numberOfSupplementaryNavBars = 2, int numberOfSupplementaryContentWidgets = 2}) {
  return Splitview(
    headerLabel: "Digestable Me",
    menuItems: SideMenuFixture.buildMenuItems(numberOfMenuItems),
    content: List<Text>.generate(numberOfContentWidgets, (i) => Text("content" + i.toString())),
    supplementaryNavBar: List<NavBar>.generate(numberOfSupplementaryNavBars, (i) => i==0 ? NavBar(middle: supplementaryNavBarMiddle) : NavBar()),
    supplementaryContent: List<Text>.generate(numberOfSupplementaryContentWidgets, (i) => Text("supplementaryContent" + i.toString())),
  );
}

Future<void> hasThreeColumns() async {
  return testWidgets('Is made up of three columns',
      (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget());
    var splitviewRow = findFirstDescendent<Row>(tester, SplitviewLayout);

    expect(splitviewRow.children.length, 3);
  });
}

Future<void> hasSideBar() async {
  return testWidgets('On the left there is a side bar',
      (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget());
    expect(find.byType(SplitViewSidebar), findsOneWidget);
  });
}

Future<void> hasSupplementaryArea() async {
  return testWidgets('In the center, a supplementary view',
      (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget());
    expect(find.byType(SplitViewSupplementary), findsOneWidget);
  });
}

Future<void> hasContentArea() async {
  return testWidgets('The content is on the right side',
      (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget());
    expect(find.byType(SplitViewContent), findsOneWidget);
  });
}

Future<void>
    assertsContentWidgetsEqualMenuItems() async {
  return testWidgets(
      'Assertion: The number of content widgets equals the number of menu items, because we require content to display for each menuitem.',
      (WidgetTester tester) async {
    var model = buildViewModel(numberOfContentWidgets: 1);

    await tester.pumpWidget(buildWidget(splitviewModel: model));

    var exceptionMessage = tester.takeException().toString();

    expect(
      exceptionMessage
          .contains(Splitview.assertionFailureNotEnoughContentWidgets),
      true,
    );
  });
}

Future<void>
    assertsSupplementaryContentWidgetsEqualMenuItems() async {
  return testWidgets(
      'Assertion: The number of supplementary content widgets equals the number of menu items, because we require content to display for each menuitem.',
      (WidgetTester tester) async {
    var model = buildViewModel(numberOfSupplementaryContentWidgets: 1);
    
    await tester.pumpWidget(buildWidget(splitviewModel: model));

    var exceptionMessage = tester.takeException().toString();

    expect(
      exceptionMessage.contains(
          Splitview.assertionFailureNotEnoughSupplementaryContentWidgets),
      true,
    );
  });
}

Future<void>
    assertsSupplementaryNavigationWidgetsEqualMenuItems() async {
  return testWidgets(
      'Assertion: The number of supplementary navigation widgets equals the number of menu items, because we require content to display for each menuitem.',
      (WidgetTester tester) async {
    var model = buildViewModel(numberOfSupplementaryNavBars: 1);

    await tester.pumpWidget(buildWidget(splitviewModel: model));

    var exceptionMessage = tester.takeException().toString();

    expect(
      exceptionMessage.contains(
          Splitview.assertionFailureNotEnoughSupplementaryNavigationWidgets),
      true,
    );
  });
}

Future<void> theContentUsesRemainingSpace() async {
  // This test is brittle as there appears to be no way to test if a widget is the parent of another.
  // It maybe that this type of test is not a good idea, this will be reviewed as part of the
  // blog on testing and test first.
  return testWidgets('Content: Expands to fill the remaining space',
      (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget());

    var expandedFinder = find.descendant(
        of: find.byType(SplitViewContent), matching: find.byType(Expanded));

    expect(expandedFinder, findsOneWidget);
  });
}

Future<void> sideBarBackgroundIsLightGrey() async {
  return testWidgets('Sidebar: Background color is light grey',
      (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget());
    var sideBarDecoration =
        findFirstDescendent<Container>(tester, SplitViewSidebar)
            .decoration
            .toString();
    expect(sideBarDecoration.containsIgnoreCase("Color(0xFFEFEFF4)"), true);
  });
}

Future<void> sideBarBackgroundIsDarkGreyInDarkMode() async {
  return testWidgets(
      'Sidebar: Background color is dark translucent gray in dark mode',
      (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget(brightness: Brightness.dark));
    var sideBarDecoration =
        findFirstDescendent<Container>(tester, SplitViewSidebar)
            .decoration
            .toString();
    expect(sideBarDecoration.containsIgnoreCase("Color(0xF01D1D1D)"), true);
  });
}

Future<void> sideBarHasThinRightBorder() async {
  return testWidgets('Sidebar: Has a thin border line on the right',
      (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget());

    var expectedDecoration = BoxDecoration(
      color: cupertinoLight.barBackgroundColor,
      border: const Border(
        right: BorderSide(
          width: 0.0,
          style: BorderStyle.solid,
          color: Color(0x4d000000),
        ),
      ),
    );

    var sideBarContainer =
        findFirstDescendent<Container>(tester, SplitViewSidebar);

    expect(sideBarContainer.decoration, expectedDecoration);
  });
}

Future<void> sideBarDisplaysMenuItems() async {
  // This test is brittle as there appears to be no way to test if a widget is the parent of another.
  // It maybe that this type of test is not a good idea, this will be reviewed as part of the
  // blog on testing and test first.
  return testWidgets("Sidebar: Displays the menu items e.g. 'Digests', 'Friends' & 'History' for navigating content. They serve the same purpose as tabs on an iPhone.",
      (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget());

    var expandedFinder = find.descendant(
        of: find.byType(SplitViewSidebar), matching: find.byType(SideMenuItems));

    expect(expandedFinder, findsOneWidget);
  });
}
///sideBarDisplaysMenuItems

Future<void> supplementaryColumnHasANavBar() async {
  return testWidgets('SupplementaryView: Has an navigation bar at the top',
      (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget());
    var navBarFinder = find.descendant(
        of: find.byType(SplitViewSupplementary),
        matching: find.byType(SplitViewNavBar));
    expect(navBarFinder, findsWidgets);
  });
}

Future<void> supplementaryColumnNavBarHasALabel() async {
  return testWidgets("SupplementaryView: The navigation bar displays a label, this is the column title and should describe its content e.g. 'Recipes'",
      (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget());
    expect(find.text(supplementaryNavBarLabel), findsOneWidget);
  });
}

Future<void> supplementaryColumnNavBarIsExtraLightGrey() async {
  return testWidgets(
      'SupplementaryView: The navigation bar background color is extra light grey',
      (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget());

    var supplementaryColumnNavBar =
        findFirstDescendent<SplitViewNavBar>(tester, SplitViewSupplementary);

    expect(supplementaryColumnNavBar.navBar.backgroundColor,
        const Color(0xFFEFEFF4).lighten(amount: 0.02));
  });
}

Future<void> supplementaryColumnNavBarWithoutContentIsHidden() async {
  return testWidgets(
      'SupplementaryView: A navigation bar without content is hidden, this allows some menu items to have navigation bars and others not.',
      (WidgetTester tester) async {
   
    String menuItemLabel = "label2";
    await tester.pumpWidget(buildWidget());
    var menuItem = find.text(menuItemLabel);
    await tester.tap(menuItem);
    await tester.pumpAndSettle();
     
    var navBarVisibilityWidget = findFirstDescendent<Visibility>(tester, SplitViewSupplementary);

    expect(navBarVisibilityWidget.visible, false);
  });
}

Future<void> supplementaryColumnDisplaysTheContent() async {
  return testWidgets('SupplementaryView: The area below the navigation bar is used to display content, like a list of recipes that is then used to select the recipe displayed in the main content area.',
      (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget());
    expect(find.text("supplementaryContent0"), findsOneWidget);
  });
}

Future<void> usesAnIndexedStackToSwitchSupplementaryContentOut() async {
  return testWidgets(
      'SupplementaryView: When there are multiple widgets, it sets up an indexed stack to switch content displayed based on the menu item selected.',
      (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget());

    var stackFinder = find.descendant(
        of: find.byType(SplitViewSupplementary),
        matching: find.byType(IndexedStack));

    var twoIndexStacksExpectedOneForContentAndOneForNavbars = 2;
    expect(stackFinder, findsNWidgets(twoIndexStacksExpectedOneForContentAndOneForNavbars));
  });
}

Future<void> contentColumnDisplaysTheContent() async {
  return testWidgets('Content: Displays the main content e.g. recipe, usually based on the item selected in the supplementary column.',
      (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget());
    expect(find.text("content1"), findsOneWidget);
  });
}

Future<void> contentBackgroundIsGrey() async {
  return testWidgets('Content: The background color is grey',
      (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget());
    var content = findFirstDescendent<Container>(tester, SplitViewContent);
    expect(content.color, const Color(0xFFE5E5EA));
  });
}

Future<void> usesAnIndexedStackToSwitchContentOut() async {
  return testWidgets(
      'Content: When there are multiple content widgets, it sets up an indexed stack to switch content displayed based on the menu item selected.',
      (WidgetTester tester) async {
   
    await tester.pumpWidget(
        buildWidget());
    var stackFinder = find.descendant(
        of: find.byType(SplitViewContent), matching: find.byType(IndexedStack));

    expect(stackFinder, findsOneWidget);
  });
}

Future<void> listensForMenuItemTapEvents() async {
  return testWidgets(
      "It listens for 'MenuItemTapped' events and sets the selected menu item which causes the content displayed to be switched out based the selection.",
      (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget());

    String menuItemLabel = "label2";

    var viewModel = buildViewModel();

    var sut = Build.app(buildWidget(splitviewModel: viewModel));

    await tester.pumpWidget(sut);
    await tester.tap(find.text(menuItemLabel));
    await tester.pump();

    expect(viewModel.selectedIndex, 1);
  });
}
