/*
Tabbed Layout - Content area tests.

Done
7. There will be a **Content Area** above the above the **Tab Bar**
9. When a Tab is pressed it displays the appropriate Content
10. Tab Items and Content are configurable I.e. inputs.
*/

// Flutter imports:
import 'package:digestableme/widget/ios/tab/tabbed_layout.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_test/flutter_test.dart';

import '../../../../fixtures/build.dart';

buildWidget(){
  return Build.app(TabbedLayout(Build.buildTabItems(2)));
}


void main() {
  group('Tabbed Layout, Content: ', () {
    hasContentArea();
    canChangeContent();
  });
}

Future<void> hasContentArea() async {
  return testWidgets('Has a Content Area', (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget());
    expect(find.byKey(const Key("TabbedLayoutContentArea0")), findsOneWidget);
  });
}

Future<void> canChangeContent() async {
  return testWidgets('Changes Content Area when a tab is pressed', (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget());
    await tester.tap(find.text('label1'));
    await tester.pump();
    expect(find.byKey(const Key("TabbedLayoutContentArea1")), findsOneWidget);
  });
}
