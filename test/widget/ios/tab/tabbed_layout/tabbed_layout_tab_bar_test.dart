/*
Tabbed Layout - Tab Bar tests.

Acceptance Criteria covered by the tests.
1. It will contain a **Tab Bar**
2. The **Tab Bar** can contain 2-5 items
5. Each item will comprise of a title and an icon
10. Tab Items and Content are configurable I.e. inputs.
*/

// Flutter imports:
import 'package:digestableme/widget/ios/tab/tabbed_layout.dart';
import 'package:flutter/cupertino.dart';

// Package imports:
import 'package:flutter_test/flutter_test.dart';

import '../../../../fixtures/build.dart';

buildWidget(){
  return Build.app(TabbedLayout(Build.buildTabItems(2)));
}

void main() {
  group('Tabbed Layout, Tab Bar: ', () {
    hasTabBar();
    tabItemsArePassedIn();
    errorsWhenNoTabItems();
    errorsWhenSixTabItems();
  });
}

Future<void> hasTabBar() async {
  return testWidgets('Has a Tab Bar', (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget());
    expect(find.byKey(const Key("TabbedLayoutTabBar")), findsOneWidget);
  });
}

Future<void> tabItemsArePassedIn() async {
  return testWidgets('The Tab Items are passed in', (WidgetTester tester) async {
    await tester.pumpWidget(buildWidget());
    expect(find.byIcon(CupertinoIcons.group), findsWidgets);
  });
}

Future<void> errorsWhenNoTabItems() async {
  return testWidgets('Errors when no tab items added',
      (WidgetTester tester) async {
    expect(() => TabbedLayout(const []), throwsA(isA<AssertionError>()));
  });
}

Future<void> errorsWhenSixTabItems() async {
  return testWidgets('Errors when more than 5 tab items added',
      (WidgetTester tester) async {
    expect(() => TabbedLayout(Build.buildTabItems(6)),
        throwsA(isA<AssertionError>()));
  });
}

