/*
Tabbed Layout - Tab Item tests.

Done
2. The **Tab Bar** can contain 2-5 items
5. Each item will comprise of a title and an icon
*/

// Flutter imports:
import 'package:flutter/cupertino.dart';

// Package imports:
import 'package:flutter_test/flutter_test.dart';

// Project imports:
import 'package:digestableme/widget/ios/tab/tabbed_layout.dart';

import '../../../../fixtures/build.dart';

void main() {
  group('Tabbed Layout, Tab Item: ', () {
    displaysName();
    displaysIcon();
  });
}

Future<void> displaysName() async {
  return testWidgets('Displays the tab name', (WidgetTester tester) async {
    await tester.pumpWidget(buildTabs());
    expect(find.text("label1"), findsOneWidget);
  });
}

Future<void> displaysIcon() async {
  return testWidgets('Displays the tab icon', (WidgetTester tester) async {
    await tester.pumpWidget(buildTabs());
    expect(find.byIcon(CupertinoIcons.group),
        findsNWidgets(3));
  });
}

Widget buildTabs() {
  return Build.app(TabbedLayout(Build.buildTabItems(3)));
}
