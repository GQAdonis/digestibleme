import 'package:flutter/widgets.dart';
import 'package:flutter_test/flutter_test.dart';

T findFirstDescendent<T extends Widget>(WidgetTester tester, Type parentType) =>
    tester.widget<T>(find
        .descendant(of: find.byType(parentType), matching: find.byType(T))
        .first);
