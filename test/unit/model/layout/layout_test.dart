/*
  Layout - Information about the layout which is device dependent.
*/

// Package imports:
import 'package:digestableme/model/layout.dart';
import 'package:flutter_test/flutter_test.dart';

import '../config/device/device_test/fixture/device_fixture.dart';

void main() {
  group('Layout: ', () {
    defaultsToLoading();
    usesATabbedLayoutForAniPhone();
    usesASplitviewLayoutForAniPad();
  });
}

Future<void> defaultsToLoading() async {
  return test('Defaults to a layout that indicates the app is loading.', () {
    expect(Layout().isTabbed || Layout().isSplitView, false);
  });
}

Future<void> usesATabbedLayoutForAniPhone() async {
  return test('Uses a tabbed layout for an iPhone.', () {
    var layout = Layout(device: DeviceFixtureIPhone.buildDevice());
    expect(layout.isTabbed, true);
  });
}

Future<void> usesASplitviewLayoutForAniPad() async {
  return test('Uses a split view layout for an iPad.', () {
    var layout = Layout(device: DeviceFixtureIPad.buildDevice());
    expect(layout.isSplitView, true);
  });
}