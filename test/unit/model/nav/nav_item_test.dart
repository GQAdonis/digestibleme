/*
   NavItem - A navigation item used to supply content to the Navigation bars used in the application.

   Usage:
   heroTag: "RecipeTabHeroTag",
          transitionBetweenRoutes: false,
          middle: const Text(
            "Recipies",
          ),
*/

import 'package:digestableme/model/nav/nav_item.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_test/flutter_test.dart';

//
// #     #                   #######                            
// #     # #    # # #####       #    ######  ####  #####  ####  
// #     # ##   # #   #         #    #      #        #   #      
// #     # # #  # #   #         #    #####   ####    #    ####  
// #     # #  # # #   #         #    #           #   #        # 
// #     # #   ## #   #         #    #      #    #   #   #    # 
//  #####  #    # #   #         #    ######  ####    #    ####  
//

//  ###### 
// #     # #    # #    # 
// #     # #    # ##   #
// ######  #    # # #  #
// #   #   #    # #  # #
// #    #  #    # #   ##
// #     #  ####  #    #

void main() {
    group('NavItem: ', () {
        leadingWidget();
        middleWidget();
        trailingWidget();
        heroTab();
    });
}

// #######
//    #    ######  ####  #####
//    #    #      #        #
//    #    #####   ####    #
//    #    #           #   #
//    #    #      #    #   #
//    #    ######  ####    #

Future<void> leadingWidget() async {
   return test("Has a leading widget for navigation controls like back '<'.", () {
      const expectedWidget =  Text("< Go back");
      expect(NavItem(leading: expectedWidget).leading, expectedWidget);
   });
}

Future<void> middleWidget() async {
   return test("Has a middle widget to display contextual information like a title 'Recipes'", () {
      const expectedWidget =  Text("Recipes");
      expect(NavItem(middle: expectedWidget).middle, expectedWidget);
   });
}

Future<void> trailingWidget() async {
   return test("Has a trailing widget for actions like add '+'", () {
      const expectedWidget =  Text("+");
      expect(NavItem(trailing: expectedWidget).trailing, expectedWidget);
   });
}

Future<void> heroTab() async {
   return test("Has a hero tag to enable transition animations.", () {
      const heroTag =  "MyHero";
      expect(NavItem(heroTag: heroTag).heroTag, heroTag);
   });
}