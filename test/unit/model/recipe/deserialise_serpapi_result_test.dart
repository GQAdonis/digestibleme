/*
  Recipe - Deserialie from json tests.
*/

// Dart imports:
import 'dart:core';

// Package imports:
import 'package:test/test.dart';

// Project imports:
import 'package:digestableme/data/api_client.dart';
import 'package:digestableme/model/recipe/recipe.dart';

void main() {
  group('Recipe, Deserialise SerpAPI result: ', () {
    mapsRecipes();
    mapsTitle();
    mapsLink();
    mapsSource();
    mapsTotalTime();
    mapsThumbnail();
    mapsIngredients();
  });
}

List<Recipe> getResult(){
   var apiClient = ApiClient();
   return Recipe.fromSerpApiResultJson(apiClient.get("/recipe/search?q=Roast+potatoes+with+balsamic").data);
}

Future<void> mapsRecipes() async {
  return test('Maps recipes', () {
    expect(getResult().length, 2);
  });
}

Future<void> mapsTitle() async {
  return test('Maps recipe title', () {
    expect(getResult()[0].title, "Balsamic potatoes");
  });
}

Future<void> mapsLink() async {
  return test('Maps recipe link', () {
    expect(getResult()[0].link, "https://www.jamieoliver.com/recipes/potato-recipes/balsamic-potatoes/");
  });
}

Future<void> mapsSource() async {
  return test('Maps recipe source', () {
    expect(getResult()[0].source, "Jamie Oliver");
  });
}

Future<void> mapsTotalTime() async {
  return test('Maps recipe total time', () {
    expect(getResult()[0].totalTime, "2 hrs 20 mins");
  });
}

Future<void> mapsThumbnail() async {
  return test('Maps recipe thumbnail', () {
    expect(getResult()[0].thumbnail, "https://serpapi.com/searches/61eed54f14b24562eddf4b80/images/ec8531e873174d51af0b6c757606e3311f60ec591c85fba6bc67c0fac6779a08.jpeg");
  });
}

Future<void> mapsIngredients() async {
  return test('Maps recipe ingredients', () {
    expect(getResult()[0].ingredients.length, 5);
  });
}
