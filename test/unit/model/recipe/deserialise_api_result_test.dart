/*
  Recipe - Deserialie from json tests.
*/

// Dart imports:
import 'dart:core';

// Package imports:
import 'package:test/test.dart';

// Project imports:
import 'package:digestableme/data/api_client.dart';
import 'package:digestableme/model/recipe/recipe.dart';

void main() {
  group('Recipe, Deserialise API result: ', () {
    mapsRecipes();
    mapsTitle();
    mapsLink();
    mapsSource();
    mapsThumbnail();
    mapsIngredients();
    mapsSteps();
  });
}

List<Recipe> getResult(){
   var apiClient = ApiClient();
   return Recipe.fromResultJson(apiClient.get("/recipe").data["Recipies"]);
}

Future<void> mapsRecipes() async {
  return test('Maps recipes', () {
    expect(getResult().length, 1);
  });
}

Future<void> mapsTitle() async {
  return test('Maps recipe title', () {
    expect(getResult()[0].title, "How to boil an egg");
  });
}

Future<void> mapsLink() async {
  return test('Maps recipe link', () {
    expect(getResult()[0].link, "https://www.theguardian.com/lifeandstyle/2014/nov/11/how-to-boil-an-egg-the-heston-blumenthal-way");
  });
}

Future<void> mapsSource() async {
  return test('Maps recipe source', () {
    expect(getResult()[0].source, "Heston Blumenthal");
  });
}

Future<void> mapsThumbnail() async {
  return test('Maps recipe thumbnail', () {
    expect(getResult()[0].thumbnail, "https://i.guim.co.uk/img/static/sys-images/Guardian/Pix/pictures/2014/11/5/1415205733799/4bfbd71a-6cd0-4494-833f-eaaed20a15b3-1020x612.jpeg?width=620&quality=45&auto=format&fit=max&dpr=2&s=ca3a95d7e761d267eff1b79b58cc4849");
  });
}

Future<void> mapsIngredients() async {
  return test('Maps recipe ingredients', () {
    var recipe = getResult()[0];
    expect(recipe.ingredients.length, 1);
  });
}

Future<void> mapsSteps() async {
  return test('Maps recipe instructions', () {
    var steps = getResult()[0].steps;
    expect(steps.length, 3);
  });
}