/*
   WidgetExtention
*/

import 'package:digestableme/core/widget/widget_extension.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_test/flutter_test.dart';

//
// #     #                   ####### 
// #     # #    # # #####       #    ######  ####  ##### 
// #     # ##   # #   #         #    #      #        #   
// #     # # #  # #   #         #    #####   ####    #   
// #     # #  # # #   #         #    #           #   #   
// #     # #   ## #   #         #    #      #    #   #   
//  #####  #    # #   #         #    ######  ####    #   
//

// #######
//    #    ######  ####  #####
//    #    #      #        #
//    #    #####   ####    #
//    #    #           #   #
//    #    #      #    #   #
//    #    ######  ####    #

void main() {
    group('WidgetExtention: ', () {
        convertsWidgetsToAnIndexedStack();
        convertsASingleWidgetToAnIndexedStack();
        convertsANullListToAContainer();
        convertsAnEmptyListToAContainer();
    });
}

Future<void> convertsWidgetsToAnIndexedStack() async {
  return test('toIndexedStack: Returns an indexed stack for multiple widgets', () {
    List<Widget>? wigets = [const Text("1"), const Text("2")];
    expect(wigets.toIndexedStack(1).runtimeType, IndexedStack);
  });
}

Future<void> convertsASingleWidgetToAnIndexedStack() async {
  return test('toIndexedStack: Returns an indexed stack for a single widget', () {
    List<Widget>? wigets = [const Text("1")];
    expect(wigets.toIndexedStack(1).runtimeType, IndexedStack);
  });
}

Future<void> convertsANullListToAContainer() async {
  return test('toIndexedStack: Returns a container when the widgets are missing (null).', () {
    List<Widget>? wigets;
    expect(wigets.toIndexedStack(1).runtimeType, Container);
  });
}


Future<void> convertsAnEmptyListToAContainer() async {
  return test('toIndexedStack: Returns a container when there are no widgets passed in', () {
    List<Widget>? wigets = [];
    expect(wigets.toIndexedStack(1).runtimeType, Container);
  });
}
