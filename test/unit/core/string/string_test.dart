/*
   stringExtension
*/

import 'package:digestableme/core/string/string_extension.dart';
import 'package:flutter_test/flutter_test.dart';

//
// #     #                   #######
// #     # #    # # #####       #    ######  ####  #####  ####
// #     # ##   # #   #         #    #      #        #   #
// #     # # #  # #   #         #    #####   ####    #    ####
// #     # #  # # #   #         #    #           #   #        #
// #     # #   ## #   #         #    #      #    #   #   #    #
//  #####  #    # #   #         #    ######  ####    #    ####
//

//  ######
// #     # #    # #    #
// #     # #    # ##   #
// ######  #    # # #  #
// #   #   #    # #  # #
// #    #  #    # #   ##
// #     #  ####  #    #

void main() {
  group('StringExtension - containsIgnoreCase: ', () {
    findLowerCaseValue();
    findMixedCaseValue();
    findUpperCaseValue();
    noMatchWhenMissing();
  });
}

// #######
//    #    ######  ####  #####
//    #    #      #        #
//    #    #####   ####    #
//    #    #           #   #
//    #    #      #    #   #
//    #    ######  ####    #

Future<void> findLowerCaseValue() async {
  return test('Matches a lower case value e.g. "me" matches "Me" ', () {
    expect("findMeImHiding".containsIgnoreCase("me"), true);
  });
}

Future<void> findMixedCaseValue() async {
  return test('Matches a mixed case value e.g. "mE" matches "Me" ', () {
    expect("findMeImHiding".containsIgnoreCase("mE"), true);
  });
}

Future<void> noMatchWhenMissing() async {
  return test(
      'Fails to match when the value is missing e.g. "MEh" does not match "Me" ',
      () {
    expect("findMeImHiding".containsIgnoreCase("MEh"), false);
  });
}

Future<void> findUpperCaseValue() async {
  return test('Matches an upper case value e.g. "ME" matches "Me" ', () {
    expect("findMeImHiding".containsIgnoreCase("ME"), true);
  });
}
