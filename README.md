# DigestibleMe

A sharable collection of things of interest, music, food, travel, sport.

# Blogs

You can find a series of blogs about this project at [Medium](https://medium.com/@simbu) or on my [WordPress Site](https://simonburgoyne.com/blog/2021/11/17/flutter-lets-go/).

## As Is

Except as represented in this agreement, all work product by Developer is provided ​“AS IS”. Other than as provided in this agreement, Developer makes no other warranties, express or implied, and hereby disclaims all implied warranties, including any warranty of merchantability and warranty of fitness for a particular purpose.

## Flutter

Resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
