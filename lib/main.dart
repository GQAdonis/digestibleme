// Flutter imports:
import 'package:digestableme/adapter/device_adapter.dart';
import 'package:digestableme/model/layout.dart';
import 'package:digestableme/theme/themer.dart';
import 'package:digestableme/widget/scaffold/layout_selector.dart';
import 'package:flutter/cupertino.dart';

// Package imports:
import 'package:provider/provider.dart';

// Project imports:
import 'package:digestableme/data/memory_repository.dart';

/*
  Application entry point
*/

void main() {
  //var deviceAdapter = DeviceAdapter();
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider<MemoryRepository>(
          create: (_) => MemoryRepository(),
          lazy: false,
        ),
        ChangeNotifierProvider<Layout>(
          create: (_) => Layout(),
          lazy: false,
        ),
        ChangeNotifierProvider<Themer>(
          create: (_) => Themer(
            brightnessValue: Brightness.light,
            whiteLabelValue: WhiteLabel.none,
          ),
          lazy: false,
        )
      ],
      child: const DigestableMe(),
    ),
  );
}

class DigestableMe extends StatelessWidget {
  const DigestableMe({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    DeviceAdapter().getDevice().then(
          (device) => {
            Provider.of<Layout>(
              context,
              listen: false,
            ).setDevice(device),
          },
        );

    return CupertinoApp(
      title: 'DigestableMe',
      theme: Provider.of<Themer>(context).themeData,
      home: const LayoutSelector(),
    );
  }
}
