import 'package:flutter/widgets.dart';

class MenuItemTapped extends Notification {
  final String label;

  const MenuItemTapped(this.label);
}