// Flutter imports:
import 'package:flutter/widgets.dart';

class TabItem {

  // The icon that is displayed
  final Icon icon;

  // The label that is displayed below the icon. Optional on Apple devices, required by material design. 
  final String? label;

  // The content displayed when the tab is selected.
  final Widget content;

  // The global key used for navigation.
  final GlobalKey<NavigatorState> globalKey;
  
  TabItem({required this.icon, this.label, required this.content, required this.globalKey});

  @override
  String toString() => 'TabItem(label: $label, icon: $icon)';
}
