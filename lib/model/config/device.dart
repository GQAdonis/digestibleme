/* 
  A Device. Used to get information about the current client device.
*/

class Device {
  final String name;
  final String model;
  final String version;
  final String identifier;

  Device({
    required this.name,
    required this.version,
    required this.identifier,
    required this.model,
  });

  get isIpad => model.toLowerCase().contains("ipad");
}
