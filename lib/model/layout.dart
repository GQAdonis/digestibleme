/* 
  Information about the layout, requires a device to make its decisions.
*/

import 'package:digestableme/model/config/device.dart';
import 'package:flutter/widgets.dart';

class Layout with ChangeNotifier {
  final Device _defaultDevice = Device(name: "", version: "", identifier: "", model: "");
  late Device? device;
  
  Layout({this.device}){
    device ??= _defaultDevice;
  }

  get isTabbed => device!.name.toLowerCase().contains("iphone");

  get isSplitView => device!.name.toLowerCase().contains("ipad");

  setDevice(Device device) {
    this.device = device;
    notifyListeners();
  }
}