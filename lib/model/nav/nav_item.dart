/*
  NavItem - A navigation item used to supply content to the Navigation bars used in the application.

  ✓ NavItem:  Has a leading widget for navigation controls like back '<'.
  ✓ NavItem:  Has a middle widget to display contextual information like a title 'Recipes'
  ✓ NavItem:  Has a trailing widget for actions like add '+'
  ✓ NavItem:  Has a hero tag to enable transition animations.

*/

import 'package:flutter/widgets.dart';

class NavItem{
  final Widget? leading;
  final Widget? middle;
  final Widget? trailing;
  final Object heroTag;

  NavItem({this.leading, this.middle, this.trailing, this.heroTag = ""});
}