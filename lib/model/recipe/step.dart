/* 
  A recipe step, a recipe can have many e.g.
     "sequenceNumber": "2",
              "instruction": "Put the pan on maximum heat with the lid on and bring to the boil."
*/

class Step {
  final int sequenceNumber;
  final String instruction;

  Step({
    required this.sequenceNumber,
    required this.instruction,
  });

  @override
  String toString() {
    //return sequenceNumber.toString() + ". " + instruction;
    return instruction;
  }
}
