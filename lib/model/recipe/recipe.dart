/*
  Recipe

    "title": "Balsamic potatoes",
    "link": "https://www.jamieoliver.com/recipes/potato-recipes/balsamic-potatoes/",
    "source": "Jamie Oliver",
    "total_time": "2 hrs 20 mins",
    "ingredients":
    [
      "Balsamic vinegar",
      "maris piper potatoes",
      "red onions",
      "rocket",
      "olive oil"
    ],
    "thumbnail": "https://serpapi.com/searches/61c3369cc99903747c1e643b/images/bd928f9ef521c02bbdb2df96157011d6a02d619d06f8a6e0e62bb157f48e10e5.jpeg"

*/

// Project imports:
import 'package:digestableme/model/recipe/ingredient.dart';
import 'package:digestableme/model/recipe/step.dart';

class Recipe {
  final String title;
  final String link;
  final String source;
  final String totalTime;
  final List<Ingredient> ingredients;
  final List<Step> steps;
  final String thumbnail;

  Recipe({
    required this.title,
    required this.link,
    required this.source,
    required this.totalTime,
    required this.ingredients,
    required this.steps,
    required this.thumbnail,
  });

  static _mapSteps(dynamic jsonSteps) {
    List<Step> ingredients = <Step>[];
    for (var ingredient in jsonSteps) {
      ingredients.add(Step(
        sequenceNumber: int.parse(ingredient["sequenceNumber"]),
        instruction: ingredient["instruction"],
      ));
    }
    return ingredients;
  }

  static _mapIngredients(dynamic jsonIngredients) {
    List<Ingredient> ingredients = <Ingredient>[];
    for (var ingredient in jsonIngredients) {
      ingredients.add(Ingredient(
          ingredientType: ingredient["type"],
          quantityType: ingredient["quantityType"],
          name: ingredient["name"],
          quantity: int.parse(ingredient["quantity"])));
    }
    return ingredients;
  }

  static _mapSerpIngredients(dynamic jsonIngredients) {
    List<Ingredient> ingredients = <Ingredient>[];
    for (var ingredient in jsonIngredients) {
      ingredients.add(Ingredient(
        name: ingredient,
      ));
    }
    return ingredients;
  }

  static List<Recipe> fromResultJson(dynamic jsonResult) {
    var recipes = jsonResult;
    var result = recipes
        .map<Recipe>((recipe) => Recipe(
              title: recipe['title'],
              ingredients: _mapIngredients(recipe["ingredients"]),
              steps: _mapSteps(recipe["steps"]),
              link: recipe['origin'],
              source: recipe['author'],
              thumbnail: recipe['titleImage'],
              totalTime: "",
            ))
        .toList();
    return result;
  }

  static List<Recipe> fromSerpApiResultJson(dynamic jsonResult) {
    var recipes = jsonResult['recipes_results'];
    var result = recipes
        .map<Recipe>((recipe) => Recipe(
              title: recipe['title'],
              ingredients: _mapSerpIngredients(recipe["ingredients"]),
              steps: <Step>[],
              link: recipe['link'],
              source: recipe['source'],
              thumbnail: recipe['thumbnail'],
              totalTime: recipe['total_time'],
            ))
        .toList();
    return result;
  }
}
