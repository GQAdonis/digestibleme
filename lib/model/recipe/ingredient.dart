/* 
  A recipe ingredient, a recipe can have many e.g.
    "quantity": "1",
    "quantityType": "unit",
    "name": "egg",
    "type": "Dairy"
*/

class Ingredient{
  final int? quantity;
  final String? quantityType;
  final String name;
  final String? ingredientType;

  Ingredient({
    required this.name,
    this.quantityType,
    this.ingredientType,
    this.quantity,
  });

  @override
  String toString() {
    var qty = quantity == null ? "" : quantity.toString() + " ";
    return qty + name;
  }
}