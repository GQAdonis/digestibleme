import 'dart:ui';

import 'package:digestableme/core/color/color_extension.dart';
import 'package:digestableme/model/splitview/side_menu.dart';
import 'package:digestableme/theme/themer.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

class Splitview extends ChangeNotifier {
  final String? headerLabel;
  final List<MenuItem>? menuItems;
  final List<Widget>? supplementaryContent;
  final List<Widget>? content;
  final List<NavBar>? supplementaryNavBar;

  String _selectedMenuItem = "";

  static const String assertionFailureNotEnoughContentWidgets =
      "SplitView: There are more menu items than content widgets to display.";
  static const String assertionFailureNotEnoughSupplementaryContentWidgets =
      "SplitView: There are more menu items than supplementary content widgets to display.";
  static const String assertionFailureNotEnoughSupplementaryNavigationWidgets =
      "SplitView: There are more menu items than supplementary navigation widgets to display.";

  Splitview({
    this.headerLabel,
    this.menuItems,
    this.supplementaryContent,
    this.content,
    this.supplementaryNavBar,
  });

  int get contentWidgetCount => content == null ? 0 : content!.length;

  int get menuItemCount => menuItems == null ? 0 : menuItems!.length;

  int get supplementaryContentWidgetCount => supplementaryContent == null ? 0 : supplementaryContent!.length;

  int get supplementaryNavWidgetCount => supplementaryNavBar == null ? 0 : supplementaryNavBar!.length;

  Splitview initialise(BuildContext buildContext) {
    if (supplementaryNavBar == null){
      return this;
    }
    
    for (var element in supplementaryNavBar!) {
      element.setBackgroundColor(buildContext);
    }
    
    return this;
  }

  /// The position of the selected menu item.
  int get selectedIndex {
    if (menuItems == null) {
      return 0;
    }

    var itemIndex =
        menuItems!.indexWhere((element) => element.label == _selectedMenuItem);

    return itemIndex == -1 ? 0 : itemIndex;
  }

  set selectedMenuItem(String label) {
    _selectedMenuItem = label;
    notifyListeners();
  }
}

class NavBar {
  final Widget? leading;
  final Widget? middle;
  final Widget? trailing;

  late Color backgroundColor;

  NavBar({
    this.leading,
    this.middle,
    this.trailing,
  });

  setBackgroundColor(BuildContext buildContext) {
    var themer = Provider.of<Themer>(buildContext, listen: false);

    backgroundColor = themer.themeData.barBackgroundColor;

    if (themer.brightness == Brightness.light) {
      backgroundColor = backgroundColor.lighten(amount: 0.02);
    }
  }

  bool get hasContent => leading != null || middle != null || trailing != null;
      
}
