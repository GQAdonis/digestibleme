import 'package:flutter/widgets.dart';

class SideMenuVM {
  final String headerLabel;
  final List<MenuItem> menuItems;
  SideMenuVM(this.headerLabel, this.menuItems);
}

class MenuItem {
  final Icon icon;
  final String label;
  MenuItem(this.icon, this.label);
}