// Flutter imports:
import 'package:digestableme/model/recipe/recipe.dart';
import 'package:digestableme/widget/scaffold/avatar.dart';
import 'package:flutter/widgets.dart';

class ListItemInfo {
  final Widget? leading;
  final Widget? trailing;
  final Widget? title;
  final Widget? subtitle;
  final GestureTapCallback? onTap;

  ListItemInfo(
      {this.leading, this.trailing, this.title, this.subtitle, this.onTap});

  static ListItemInfo fromRecipe(Recipe recipe, GestureTapCallback? onTap,
      {Widget? trailing}) {
    return ListItemInfo(
      leading: Avatar(
        Image.network(recipe.thumbnail),
      ),
      title: Text(recipe.title),
      subtitle: Text(recipe.source),
      onTap: onTap,
      trailing: trailing,
    );
  }
}
