import 'package:digestableme/data/tab_items.dart';
import 'package:digestableme/model/layout.dart';
import 'package:digestableme/model/nav/nav_item.dart';
import 'package:digestableme/theme/themer.dart';
import 'package:digestableme/widget/content/recipe/recipe_details.dart';
import 'package:digestableme/widget/content/recipe/recipes.dart';
import 'package:digestableme/widget/scaffold/loading_layout.dart';
import 'package:digestableme/model/splitview/splitview.dart';
import 'package:digestableme/widget/ios/splitview/splitview_layout.dart';
import 'package:digestableme/widget/ios/tab/tabbed_layout.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';

class LayoutSelector extends StatelessWidget {
  const LayoutSelector({Key? key}) : super(key: key);

  navItem(BuildContext context) {
    var themer = Provider.of<Themer>(context);
    return NavItem(
      middle: Text(
        "Recipes",
        style: themer.typo.navBar,
      ),
      trailing: GestureDetector(
        onTap: () {},
        child: const Icon(CupertinoIcons.add),
      ),
      heroTag: {},
    );
  }

  @override
  Widget build(BuildContext context) {
    var layout = Provider.of<Layout>(context);
    var supplementaryNavBar = navItem(context);
    var splitView = Splitview(
        headerLabel: "Digestable Me",
        menuItems: menuItems,
        supplementaryNavBar: [
          NavBar(
            middle: supplementaryNavBar.middle,
            trailing: supplementaryNavBar.trailing,
          ),
          NavBar(),
          NavBar(),
        ],
        supplementaryContent: [
          const Padding(
            padding: EdgeInsets.only(left: 4.0),
            child: Recipes(),
          ),
          Container(
            color: CupertinoColors.systemYellow,
          ),
          Container(
            color: CupertinoColors.systemPink,
          ),
        ],
        content: <Widget>[
          const RecipeDetails(),
          Container(
            color: CupertinoColors.activeOrange,
          ),
          Container(
            color: CupertinoColors.destructiveRed,
          ),
        ]);

    return layout.isTabbed
        ? TabbedLayout(tabItems)
        : layout.isSplitView
            ? ChangeNotifierProvider(
                create: (context) => splitView.initialise(context),
                child: const SplitviewLayout(),
              )
            : const LoadingLayout();
  }
}
