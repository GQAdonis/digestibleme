import 'package:flutter/widgets.dart';

class ScrollableContent extends StatelessWidget {
  const ScrollableContent({
    Key? key,
    required this.items,
  }) : super(key: key);

  final List<Widget> items;

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(slivers: <Widget>[
      SliverSafeArea(
        top: false,
        minimum: const EdgeInsets.only(top: 4),
        sliver: SliverList(
          delegate: SliverChildListDelegate(
            [
              for(var item in items) item
            ],
          ),
        ),
      ),
    ]);
  }
}