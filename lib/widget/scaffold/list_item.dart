/*
  Uses the Cupertino ListTile to provide a standard experience on iOS.
  It can easily be extend to support Android and other client platforms like the web.
  https://pub.dev/packages/cupertino_list_tile
  https://api.flutter.dev/flutter/material/ListTile-class.html
 */

// Flutter imports:
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// Package imports:
import 'package:cupertino_list_tile/cupertino_list_tile.dart';

// Project imports:
import '../../model/list/list_item_info.dart';

//import 'package:digestableme/ui/widget/list_item/domain/list_item_info.dart';

class ListItem extends StatelessWidget {
  final ListItemInfo item;

  const ListItem(this.item, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 4.0),
      child: CupertinoListTile(
        leading: SizedBox(child: (item.leading ?? Container()), width:  50),
        trailing: SizedBox(child: (item.trailing ?? Container()), width:  50),
        title: item.title ?? Container(),
        subtitle: item.subtitle ?? Container(),
        onTap: item.onTap
      ),
    );
  }
}
