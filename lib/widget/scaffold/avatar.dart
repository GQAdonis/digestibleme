import 'package:flutter/material.dart';

class Avatar extends StatelessWidget {
  final Image image;
  const Avatar(this.image, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      backgroundColor: Colors.black54,
      radius: 36.0,
      child: CircleAvatar(
        backgroundImage: image.image,
        radius: 35.0,
      ),
    );
  }
}