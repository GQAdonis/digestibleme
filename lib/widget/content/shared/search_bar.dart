// Flutter imports:
import 'package:digestableme/theme/themer.dart';
import 'package:flutter/cupertino.dart';

// Package imports:
import 'package:provider/provider.dart';

// Stateful because of the textEditingController, was getting an error when stateless.
class SearchBar extends StatefulWidget {
  final TextEditingController textEditingController;
  final ValueChanged<String>? onSubmitted;
  const SearchBar(this.textEditingController, this.onSubmitted, {Key? key})
      : super(key: key);

  @override
  State<SearchBar> createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> {
  @override
  Widget build(BuildContext context) {
    var themer = Provider.of<Themer>(context);
    return Padding(
      padding: const EdgeInsets.all(15),
      child: CupertinoSearchTextField(
        backgroundColor: themer.brightness == Brightness.dark ? CupertinoColors.darkBackgroundGray : CupertinoColors.extraLightBackgroundGray,
        style: themer.typo.body,
        controller: widget.textEditingController,
        placeholder: "Recipes...", 
        onSubmitted: widget.onSubmitted,
      ),
    );
  }
}
