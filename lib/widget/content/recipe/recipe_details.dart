/*
  Recipe details

  The layout is loosely based on https://www.etsy.com/uk/listing/1086273678/editable-recipe-card-template-recipe?gpla=1&gao=1&&utm_source=google&utm_medium=cpc&utm_campaign=shopping_uk_en_gb_d-paper_and_party_supplies-other&utm_custom1=_k_Cj0KCQiAip-PBhDVARIsAPP2xc19gpUnxE45VTqUeNgOLs6tWI8pbAU86tI3LYdhU-YM0Zf7Fr29I8oaApZzEALw_wcB_k_&utm_content=go_12576528914_120381161035_507694609097_pla-314261241267_c__1086273678engb_102858184&utm_custom2=12576528914&gclid=Cj0KCQiAip-PBhDVARIsAPP2xc19gpUnxE45VTqUeNgOLs6tWI8pbAU86tI3LYdhU-YM0Zf7Fr29I8oaApZzEALw_wcB
 */

// Flutter imports:
import 'package:digestableme/data/memory_repository.dart';
import 'package:digestableme/theme/themer.dart';
import 'package:digestableme/widget/scaffold/scrollable_content.dart';
import 'package:flutter/cupertino.dart';

// Package imports:
import 'package:provider/provider.dart';

class RecipeDetails extends StatelessWidget {
  const RecipeDetails({Key? key}) : super(key: key);

  static const String ingredientsTitle = "Ingredients";
  static const String stepsTitle = "Steps";

  Padding recipeTitleAndAuthor(BuildContext context, Themer themer) {
    var recipe =
        Provider.of<MemoryRepository>(context).selectedRecipe;
    return Padding(
      padding: const EdgeInsets.only(top: 8, bottom: 8),
      child: Center(
          child: Column(
        children: [
          Text(recipe!.title, style: themer.typo.title3),
          Text(recipe.source, style: themer.typo.body),
        ],
      )),
    );
  }

  Widget recipeImage(
    BuildContext context,
  ) {
    var recipe =
        Provider.of<MemoryRepository>(context).selectedRecipe;
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Image.network(
        recipe!.thumbnail,
        fit: BoxFit.fill,
      ),
    );
  }

  List<Widget> toDisplayList(
    BuildContext context,
    Themer themer,
    String title,
  ) {
    var recipe =
        Provider.of<MemoryRepository>(context).selectedRecipe;

    Iterable<String> items = title == RecipeDetails.ingredientsTitle
        ? recipe!.ingredients.map((i) => i.toString())
        : recipe!.steps.map((i) => i.toString());

    return <Widget>[
      Visibility(
        visible: title != "",
        child: Padding(
          padding: const EdgeInsets.only(top: 16.0),
          child: Center(
              child: Text(
            title,
            style: themer.typo.title3,
          )),
        ),
      ),
      for (var item in items)
        Padding(
          padding: const EdgeInsets.only(
            left: 34,
            right: 34,
            top: 10,
          ),
          child: Center(
            child: Text(
              item,
              textAlign: TextAlign.justify,
            ),
          ),
        )
    ];
  }

  List<Widget> ingredients(BuildContext context, Themer themer) {
    return toDisplayList(
      context,
      themer,
      RecipeDetails.ingredientsTitle,
    );
  }

  List<Widget> instructions(BuildContext context, Themer themer) {
    return toDisplayList(
      context,
      themer,
      "", //stepsTitle,
    );
  }

  Padding shrug() {
    return const Padding(
      padding: EdgeInsets.only(top: 10.0, bottom: 40),
      child: Center(
          child: Text(
        " ¯¯\\_(ツ)_/¯¯",
        style: TextStyle(fontSize: 35),
      )),
    );
  }

  @override
  Widget build(BuildContext context) {
    var recipe = Provider.of<MemoryRepository>(context).selectedRecipe;
    if (recipe == null) {
      return Container();
    }

    var themer = Provider.of<Themer>(context);

    return ScrollableContent(items: <Widget>[
      recipeTitleAndAuthor(context, themer),
      recipeImage(context),
      for (Widget item in ingredients(context, themer)) item,
      for (Widget item in instructions(context, themer)) item,
      shrug()
    ]);
  }
}
