// Flutter imports:
import 'package:digestableme/widget/content/shared/search_bar.dart';
import 'package:digestableme/widget/scaffold/scrollable_content.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// Package imports:
import 'package:dio/dio.dart';
import 'package:provider/provider.dart';

// Project imports:
import 'package:digestableme/data/api_client.dart';
import 'package:digestableme/data/memory_repository.dart';
import 'package:digestableme/model/recipe/recipe.dart';
import 'package:digestableme/model/list/list_item_info.dart';
import 'package:digestableme/widget/scaffold/list_item.dart';

class SearchAndAddRecipe extends StatefulWidget {
  const SearchAndAddRecipe({Key? key}) : super(key: key);

  @override
  State<SearchAndAddRecipe> createState() => _SearchAndAddRecipeState();
}

class _SearchAndAddRecipeState extends State<SearchAndAddRecipe> {
  final ApiClient _api = ApiClient();
  late TextEditingController _textController;
  late Response<dynamic> _searchResults;

  @override
  void initState() {
    super.initState();
    _textController = TextEditingController(text: '');
    _searchResults = _api.get("");
  }

  @override
  Widget build(BuildContext context) {
    //Recipe.fromSerpApiResultJson(apiClient.get("/recipe/search?q=Roast+potatoes+with+balsamic").data);
    var searchResultRecipes = _searchResults.data.length > 0
        ? Recipe.fromSerpApiResultJson(_searchResults.data)
        : <Recipe>[];

    return CupertinoPageScaffold(
      navigationBar: navBar(),
      child: ScrollableContent(items: [
        buildSearch(),
        for (var result in searchResultRecipes) searchResult(result)
      ]),
    );
  }

  Widget searchResult(Recipe result) {
    final repository = Provider.of<MemoryRepository>(context);
    var listItemInfo = ListItemInfo.fromRecipe(
      result,
      () => {repository.addRecipe(result), Navigator.pop(context)},
      trailing: const Icon(CupertinoIcons.add_circled_solid)
    );

    return ListItem(listItemInfo);
  }

  CupertinoNavigationBar navBar() {
    return const CupertinoNavigationBar(
      heroTag: "RecipeSearchAndAddHeroTag",
      transitionBetweenRoutes: false,
      middle: Text(
        "Find Recipe",
      ),
    );
  }

  Stack buildSearch() {
    return Stack(
      children: [
        Material(
          child: Image.asset(
            "assets/recipes_sm3.jpg",
            fit: BoxFit.fill,
          ),
          elevation: 27,
        ),
        SearchBar(
          _textController,
          (String value) {
            if (value.toLowerCase().contains("roast") ||
                value.toLowerCase().contains("potatoe") ||
                value.toLowerCase().contains("balsamic")) {
              setState(() {
                _searchResults =
                    _api.get("/recipe/search?q=Roast+potatoes+with+balsamic");
              });
            } else {
              setState(() {
                _searchResults = _api.get("");
              });
            }
          },
        )
        //buildSearchField(),
      ],
    );
  }
}
