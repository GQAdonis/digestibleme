import 'package:digestableme/data/memory_repository.dart';
import 'package:digestableme/model/layout.dart';
import 'package:digestableme/model/list/list_item_info.dart';
import 'package:digestableme/widget/content/recipe/recipe_details.dart';
import 'package:digestableme/widget/scaffold/list_item.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';

class Recipes extends StatelessWidget {
  const Recipes({Key? key}) : super(key: key);

  navigateToSelectedRecipe(BuildContext context) {
    return Navigator.push(
      context,
      CupertinoPageRoute(
        builder: (context) => const CupertinoPageScaffold(
          child: RecipeDetails(),
          navigationBar: CupertinoNavigationBar(
            heroTag: "RecipeDetailsHeroTag",
            transitionBetweenRoutes: false,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final repository = Provider.of<MemoryRepository>(context);
    final layout = Provider.of<Layout>(context);
    var recipes = repository.findAllRecipes();

    return ListView.builder(
      itemCount: recipes.length,
      itemBuilder: (context, index) {
        var listItemInfo = ListItemInfo.fromRecipe(recipes[index], () {
          repository.selectRecipe(recipes[index]);
          if (layout.isTabbed) {
            navigateToSelectedRecipe(context);
          }
        });

        return ListItem(listItemInfo);
      },
    );
  }
}
