// Flutter imports:
import 'package:digestableme/model/tab/tab_item.dart';
import 'package:flutter/cupertino.dart';

class TabbedLayout extends StatelessWidget {
  final List<TabItem> items;
  TabbedLayout(this.items, {Key? key}) : super(key: key){
    assert(items.length > 1);
    assert(items.length < 6);
  }

  @override
  Widget build(BuildContext context) {
    var navBarItems = List.generate(items.length, (int index) {
      return buildBottomNavBarItem(index, items);
    });
    return CupertinoTabScaffold(
      tabBar: CupertinoTabBar(
        key: const Key("TabbedLayoutTabBar"),
        items: navBarItems,
      ),
      tabBuilder: (context, index) {
        return CupertinoTabView(
          navigatorKey: items[index].globalKey,
          builder: (BuildContext context) => items[index].content,
        );
      },
    );
  }

  BottomNavigationBarItem buildBottomNavBarItem(
      int index, List<TabItem> items) {
    return BottomNavigationBarItem(
      icon: items[index].icon,
      label: items[index].label,
    );
  }
}
