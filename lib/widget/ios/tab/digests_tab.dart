// Flutter imports:
import 'package:digestableme/theme/themer.dart';
import 'package:digestableme/widget/content/recipe/recipes.dart';
import 'package:digestableme/widget/content/recipe/search_and_add_recipe.dart';
import 'package:flutter/cupertino.dart';

// Project imports:
import 'package:provider/provider.dart';

class DigestsTab extends StatelessWidget {
  const DigestsTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        heroTag: "RecipeTabHeroTag",
        transitionBetweenRoutes: false,
        middle: Text(
          "Recipes",
          style: Provider.of<Themer>(context, listen: false).typo.navBar,
        ),
        trailing: GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              CupertinoPageRoute(
                builder: (context) => const SearchAndAddRecipe(),
              ),
            );
          },
          child: const Icon(CupertinoIcons.add),
        ),
      ),
      child: const Recipes(),
    );
  }
}
