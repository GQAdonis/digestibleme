import 'package:digestableme/event/menu/menu_item_tapped.dart';
import 'package:digestableme/model/splitview/side_menu.dart';
import 'package:digestableme/theme/themer.dart';
import 'package:digestableme/widget/ios/splitview/side_menu.dart';
import 'package:digestableme/core/widget/widget_extension.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../model/splitview/splitview.dart';

class SplitViewNavBar extends StatelessWidget {
  final NavBar navBar;

  const SplitViewNavBar(this.navBar, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (!navBar.hasContent){
      return Container();
    }

    var leadingWidget = navBar.leading ?? const Text("");
    var middleWidget = navBar.middle ?? Container();
    var trailingWidget = navBar.trailing ?? Container();

    return SizedBox(
      width: double.infinity,
      //decoration: const BoxDecoration(color: CupertinoColors.activeBlue),
      child: DecoratedBox(
        decoration: BoxDecoration(color: navBar.backgroundColor),
        child: SafeArea(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              leadingWidget,
              Padding(
                padding: const EdgeInsets.only(left: 6.0),
                child: middleWidget,
              ),
              Padding(
                padding: const EdgeInsets.only(right: 6.0),
                child: trailingWidget,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class SplitViewSidebar extends StatelessWidget {
  const SplitViewSidebar({Key? key}) : super(key: key);

  Widget buildSideBar(BuildContext context) {
    var splitview = Provider.of<Splitview>(context);
    var menuItems = splitview.menuItems;
    Widget sideMenu = Container();
    if (menuItems != null) {
      sideMenu = SideMenu(SideMenuVM(splitview.headerLabel ?? "", menuItems));
    }
    return sideMenu;
  }

  @override
  Widget build(BuildContext context) {
    final themer = Provider.of<Themer>(context);
    final barBackgroundColor = themer.themeData.barBackgroundColor;
    return Container(
      width: 250,
      //color: barBackgroundColor,
      decoration: BoxDecoration(
        color: barBackgroundColor,
        border: const Border(
          right: BorderSide(
            width: 0.0,
            style: BorderStyle.solid,
            color: Color(0x4d000000),
          ),
        ),
      ),
      child: SafeArea(child: buildSideBar(context)),
    );
  }
}

class SplitViewSupplementary extends StatelessWidget {
  const SplitViewSupplementary({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final splitview = Provider.of<Splitview>(context);
    List<Widget> navBarWidgets = [];
    for (var element in splitview.supplementaryNavBar!) {
      navBarWidgets.add(SplitViewNavBar(element));
    }
    
    var supplementaryNavBar = navBarWidgets.toIndexedStack(splitview.selectedIndex);
    var content = splitview.supplementaryContent.toIndexedStack(splitview.selectedIndex);

    return Container(
      width: 350,
      color: Provider.of<Themer>(context).themeData.scaffoldBackgroundColor,
      child: Column(
        children: [
          Visibility(child: supplementaryNavBar, visible: (splitview.supplementaryNavBar![splitview.selectedIndex]).hasContent,),
          Expanded(child: content),
        ],
      ),
    );
  }
}

class SplitViewContent extends StatelessWidget {
  const SplitViewContent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final themer = Provider.of<Themer>(context);
    final splitview = Provider.of<Splitview>(context);

    var content = splitview.content.toIndexedStack(splitview.selectedIndex);

    return Expanded(
      child: Container(
        key: const Key("SplitviewContent"),
        color: themer.themeData.panelBackgroundColor,
        child: content,
      ),
    );
  }
}

class SplitviewLayout extends StatelessWidget {
  const SplitviewLayout({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final splitview = Provider.of<Splitview>(context);
    assert(splitview.contentWidgetCount >=  splitview.menuItemCount, Splitview.assertionFailureNotEnoughContentWidgets);
    assert(splitview.supplementaryContentWidgetCount >=  splitview.menuItemCount, Splitview.assertionFailureNotEnoughSupplementaryContentWidgets);
    assert(splitview.supplementaryNavWidgetCount >=  splitview.menuItemCount, Splitview.assertionFailureNotEnoughSupplementaryNavigationWidgets);
    return NotificationListener<MenuItemTapped>(
        child: Row(
          children: const [
            SplitViewSidebar(),
            SplitViewSupplementary(),
            SplitViewContent()
          ],
        ),
        onNotification: (notification) {
          splitview.selectedMenuItem = notification.label;
          return true;
        });
  }
}
