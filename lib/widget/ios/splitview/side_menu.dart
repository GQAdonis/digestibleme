import 'package:digestableme/model/splitview/side_menu.dart';
import 'package:digestableme/widget/ios/splitview/side_menu_header.dart';
import 'package:digestableme/widget/ios/splitview/side_menu_items.dart';
import 'package:flutter/widgets.dart';

class SideMenu extends StatelessWidget {
  final SideMenuVM model;
  const SideMenu(this.model, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SideMenuHeader(model.headerLabel),
        Expanded(child: SideMenuItems(model.menuItems))
      ],
    );
  }
}