import 'package:digestableme/event/menu/menu_item_tapped.dart';
import 'package:digestableme/model/splitview/side_menu.dart';
import 'package:digestableme/widget/scaffold/scrollable_content.dart';
import 'package:flutter/widgets.dart';

class SideMenuItems extends StatelessWidget {
  final List<MenuItem> menuItems;
  const SideMenuItems(this.menuItems, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScrollableContent(items: <Widget>[
      for (MenuItem item in menuItems) buildMenuItem(context, item),
    ]);
  }

  Widget buildMenuItem(BuildContext context, MenuItem item) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, bottom: 12),
      child: GestureDetector(
        child: Row(
          children: [
            item.icon,
            Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Text(item.label),
            ),
          ],
        ),
        onTap: () => MenuItemTapped(item.label)..dispatch(context),
      ),
    );
  }
}