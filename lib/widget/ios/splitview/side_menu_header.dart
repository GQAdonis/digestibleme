import 'package:flutter/widgets.dart';

class SideMenuHeader extends StatelessWidget {
  final String headerLabel;
  const SideMenuHeader(this.headerLabel, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: DecoratedBox(
          child: Padding(
            padding: const EdgeInsets.only(top: 8, left: 16, right: 16 , bottom: 16),
            child: FittedBox(
              child: Text(
                headerLabel,
                textAlign: TextAlign.start,
              ),
              fit: BoxFit.contain,
            ),
          ),
          decoration: const BoxDecoration(
            color: Color(0x00000000),
          )),
    );
  }
}
