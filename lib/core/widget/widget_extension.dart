import 'package:flutter/widgets.dart';

extension WidgetExtension on List<Widget>? {
  Widget toIndexedStack(int index) {
    if(this == null){
      return Container();
    }

    if (this!.isEmpty){
      return Container();
    }

    return IndexedStack(index: index, children: this ?? [],);
  }
}