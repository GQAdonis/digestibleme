// Flutter imports:
import 'package:digestableme/model/splitview/side_menu.dart';
import 'package:flutter/cupertino.dart';

// Project imports:
import 'package:digestableme/model/tab/tab_item.dart';
import 'package:digestableme/widget/ios/tab/digests_tab.dart';

List<TabItem> get tabItems {
  return <TabItem>[
    TabItem(
      label: "Digests",
      icon: const Icon(CupertinoIcons.bars),
      content: const DigestsTab(),
      globalKey: GlobalKey<NavigatorState>(),
    ),
    TabItem(
      label: "Friends",
      icon: const Icon(CupertinoIcons.group),
      content: Container(
        color: CupertinoColors.activeOrange,
      ),
      globalKey: GlobalKey<NavigatorState>(),
    ),
    TabItem(
      label: "History",
      icon: const Icon(CupertinoIcons.time),
      content: Container(
        color: CupertinoColors.destructiveRed,
      ),
      globalKey: GlobalKey<NavigatorState>(),
    ),
  ];
}

List<MenuItem> get menuItems {
  var items = <MenuItem>[];
  for (var tabItem in tabItems) {
    items.add(MenuItem(tabItem.icon, tabItem.label ?? ""));
  }
  return items;
}
