// Dart imports:
import 'dart:core';

// Flutter imports:
import 'package:flutter/foundation.dart';

// Project imports:
import 'package:digestableme/data/api_client.dart';
import 'package:digestableme/model/recipe/recipe.dart';

/*
  In memory data repository.

  Find, FindAll, FindBy
  Add
  Update
  Delete

  IDs need to think about add/map datastore ids or generate ones, identity of things needs more thought.
*/
class MemoryRepository extends ChangeNotifier {
  final List<Recipe> _currentRecipes = <Recipe>[];
  final ApiClient _api = ApiClient();

  Recipe? selectedRecipe;

  MemoryRepository() {
    _loadRecipes();

    //Recipe(title: "", link: "", source: "", totalTime: "", thumbnail: "", ingredients: [], steps: [])
  }

  /*
    Recipes
  */

  void _loadRecipes() {
    // Retrieve and add recipes to the in-memory store.
    var recipes = Recipe.fromResultJson(_api.get("/recipe").data["Recipies"]);
    for (var recipe in recipes) {
      _currentRecipes.add(recipe);
    }
  }

  List<Recipe> findAllRecipes() {
    return _currentRecipes;
  }

  Recipe addRecipe(Recipe recipe) {
    _currentRecipes.add(recipe);
    notifyListeners();

    return recipe;
  }

  Recipe selectRecipe(Recipe recipe){
    selectedRecipe = recipe;
    notifyListeners();
    return recipe;
  }
}
