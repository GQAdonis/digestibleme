/*
  The theme used by the application, which is choosen on startup dependent on brand and device.

  Named Themex not to clash with Theme
 */
import 'package:flutter/cupertino.dart';

class Themex extends CupertinoThemeData {
  final Color? panelBackgroundColor;
  const Themex({
    brightness,
    primaryColor,
    primaryContrastingColor,
    textTheme,
    barBackgroundColor,
    scaffoldBackgroundColor,
    this.panelBackgroundColor,
  }) : super(
          brightness: brightness,
          primaryColor: primaryColor,
          primaryContrastingColor: primaryContrastingColor,
          textTheme: textTheme,
          barBackgroundColor: barBackgroundColor,
          scaffoldBackgroundColor: scaffoldBackgroundColor,
        );
}
