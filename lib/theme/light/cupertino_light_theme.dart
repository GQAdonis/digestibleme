// Flutter imports:
import 'package:digestableme/theme/themex.dart';
import 'package:flutter/cupertino.dart';

Themex cupertinoLight = const Themex(
  brightness: Brightness.light,
  primaryColor: CupertinoColors.activeBlue,
  primaryContrastingColor: CupertinoColors.black,
  barBackgroundColor: Color(0xFFEFEFF4), //CupertinoColors.extraLightBackgroundGray, //Color(0xF0F9F9F9), //Nav & tab bar, light grey.
  scaffoldBackgroundColor: CupertinoColors.systemBackground, //Main area
  panelBackgroundColor: CupertinoColors.lightBackgroundGray
);