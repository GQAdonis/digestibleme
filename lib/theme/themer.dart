/*
  Application Theme provider
*/
import 'package:digestableme/theme/themex.dart';
import 'package:flutter/cupertino.dart';

import 'package:digestableme/theme/typo.dart';
import 'package:digestableme/theme/dark/cupertino_dark_theme.dart';
import 'package:digestableme/theme/light/cupertino_light_theme.dart';
import 'package:digestableme/theme/whitelabel/ee/ee_dark_theme.dart';
import 'package:digestableme/theme/whitelabel/ee/ee_light_theme.dart';

/*
  Whitelabel
*/
enum WhiteLabel {
  none,
  ee,
}

// Theme provider
class Themer with ChangeNotifier {
  late Brightness brightness = Brightness.light;
  late WhiteLabel whitelabel = WhiteLabel.none;

  late Typo _typo;
  late Themex _themex;

  Themer({Brightness? brightnessValue, WhiteLabel? whiteLabelValue}){
    if(brightnessValue != null){
      brightness = brightnessValue;
    }
    if(whiteLabelValue != null){
      whitelabel = whiteLabelValue;
    }
    _setTheme();
    _typo = Typo();
  }

  Themex get themeData {
    return _themex;
  }

  Typo get typo {
    return _typo;
  }

  void setBrightness(Brightness brightnessValue){
    brightness = brightnessValue;
    setThemeAndNotify();
  }

  void setWhitelabel(WhiteLabel whiteLabelValue){
    whitelabel = whiteLabelValue;
    setThemeAndNotify();
  }

  void setThemeAndNotify({Brightness? brightnessValue, WhiteLabel? whiteLabelValue}){
    if (brightnessValue != null) {
      brightness = brightnessValue;
    }
    if (whiteLabelValue != null) {
      whitelabel = whiteLabelValue;
    }
    _setTheme();
    notifyListeners();
  }

  void _setTheme(){
    var themex = whitelabel == WhiteLabel.ee && brightness == Brightness.dark
        ? eeDark
        : whitelabel == WhiteLabel.ee
            ? eeLight
            : brightness == Brightness.dark
                ? cupertinoDark
                : cupertinoLight;
    _themex = themex;
  }
}
