// Flutter imports:
import 'package:digestableme/theme/themex.dart';
import 'package:flutter/cupertino.dart';

// Project imports:
import 'package:digestableme/theme/whitelabel/ee/ee_swatch.dart';

const Themex eeLight = Themex(
  brightness: Brightness.light,
  primaryColor: eeSwatch.yellow,
  primaryContrastingColor: eeSwatch.darkGrey,
  barBackgroundColor: eeSwatch.aqua, //Nav & tab bar.
  textTheme: CupertinoTextThemeData(
    navTitleTextStyle: TextStyle(fontFamily: "NobbleeLight", fontSize: 22),
    navActionTextStyle: TextStyle(color: eeSwatch.yellow),
    textStyle: TextStyle(fontFamily: "Rubrik", fontSize: 14, color: eeSwatch.darkGrey)
  ),
  panelBackgroundColor: eeSwatch.onlineAqua
);
