// Flutter imports:
import 'package:digestableme/theme/themex.dart';
import 'package:flutter/cupertino.dart';

const Themex cupertinoDark = Themex(
  brightness: Brightness.dark,
  primaryColor: CupertinoColors.activeGreen,
  primaryContrastingColor: CupertinoColors.white,
  barBackgroundColor: Color(0xF01D1D1D), //Nav & tab bar, dark translucent gray 
  scaffoldBackgroundColor: CupertinoColors.black, //Main area
  panelBackgroundColor: CupertinoColors.black,
);